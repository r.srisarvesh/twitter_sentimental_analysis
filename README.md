# Twitter_Sentimental_Analysis
*In this Project I have created sentimental analyser which is used to classify the mood of the tweets.

*We initially created a python bot that uses the Access Credentials of My twitter Developer account and retrives the tweets.

*Then I have created a sentimentalanalyser model using the datas from positive_tweets.json,negative_tweets.json files (That i retrived using my api that i have created using bot)

*Using the sentimentalanalyser model inside the ProcessTwitterDump.py to classify the moods of different tweets.

*In this case I have taken datas of tweets_with_java.json and tweets_with_python.json as a input files and i send those files to the sentimental analyser model and that model classifies the tweet of the mood either to positive or negative and it also retrives the latitude and longitude locations of user who tweeted the tweet and store all these details in a csv file.

*Then I have used the two csv output files and created a file CreateMap.py that loads the csv files.For each of the csv file it creates an html file in which it describes a map that has all the countries represented.And each country is colored based upon the mood of the tweet about java and python.

*There are  two html files  each represents a map that holds various countries along with thier mood in various colors about java and python.
